<?php
session_start();

if (array_key_exists("winner", $_REQUEST) && array_key_exists($_REQUEST["winner"], $_SESSION)) {
    $_SESSION[$_REQUEST["winner"]]["wins"]++;
}

header("Location: ./");