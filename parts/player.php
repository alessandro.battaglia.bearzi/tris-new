<form class="row">
    <div class="col-12">
        <p class="text-muted">
            Inserisci i nomi dei giocatori prima di iniziare a giocare:
        </p>
    </div>
    <div class="col">
        <div class="form-group">
            <label for="playerTwoName">Nome giocatore uno</label>
            <input type="text" class="form-control" id="playerTwoName" aria-describedby="playerOneNameHelp" required
                   name="player_one">
            <small id="playerOneNameHelp" class="form-text text-muted">Inserisci il nome del giocatore uno.</small>
        </div>
    </div>
    <div class="col">
        <div class="form-group">
            <label for="playerOneName">Nome giocatore due</label>
            <input type="text" class="form-control" id="playerOneName" aria-describedby="playerTwoNameHelp" required
                   name="player_two">
            <small id="playerTwoNameHelp" class="form-text text-muted">Inserisci il nome del giocatore due.</small>
        </div>
    </div>
    <div class="col-12">
        <button type="submit" class="btn btn-primary w-100 mt-2">Conferma</button>
    </div>
</form>