<div class="d-flex">
    <div id="player_one" class="w-100 text-center">
        <div>
            <small class="text-muted">Nome:</small>
            <?= $_SESSION['player_one']['name'] ?>
        </div>
        <div>
            <small class="text-muted">Vincite:</small>
            <?= $_SESSION['player_one']['wins'] ?>
        </div>
        <div>
            <small class="text-muted">Segno:</small>
            <img src="svg/x.svg" alt="x" height="20"/>
        </div>
    </div>
    <div class="w-100 d-flex flex-column" id="game">
        <div id="table" class="m-auto">
            <?php for ($i = 0; $i < 3; $i++) { ?>
                <div class="d-flex">
                    <?php for ($j = 0; $j < 3; $j++) { ?>
                        <div class="cell border d-flex" data-row="<?= $i ?>" data-col="<?= $j ?>"></div>
                    <?php } ?>
                </div>
            <?php } ?>
        </div>
        <p id="round" class="mt-3 text-center">aef</p>
    </div>
    <div id="player_two" class="w-100 text-center">
        <div>
            <small class="text-muted">Nome:</small>
            <?= $_SESSION['player_two']['name'] ?>
        </div>
        <div>
            <small class="text-muted">Vincite:</small>
            <?= $_SESSION['player_two']['wins'] ?>
        </div>
        <div>
            <small class="text-muted">Segno:</small>
            <img src="svg/o.svg" alt="x" height="20"/>
        </div>
    </div>
</div>

<style>
    .cell {
        width: 100px;
        height: 100px;
    }

    .cell:hover,
    .cell.used {
        border-color: black !important;
        cursor: pointer;
    }
</style>