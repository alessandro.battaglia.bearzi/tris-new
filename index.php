<?php
session_start();

if (array_key_exists("player_one", $_REQUEST) && array_key_exists("player_two", $_REQUEST)) {
    $_SESSION["player_one"] = [
        "name" => $_REQUEST["player_one"],
        "wins" => 0,
    ];
    $_SESSION["player_two"] = [
        "name" => $_REQUEST["player_two"],
        "wins" => 0,
    ];

    header("Location: ./");
}

$logged = array_key_exists("player_one", $_SESSION) && array_key_exists("player_two", $_SESSION);
?>


<!doctype html>
<html lang="it">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <!-- FontAwesome CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.0/css/all.min.css"
          integrity="sha256-ybRkN9dBjhcS2qrW1z+hfCxq+1aBdwyQM5wlQoQVt/0=" crossorigin="anonymous"/>

    <title>Tris the new version</title>
</head>
<body>

<div class="container min-vh-100">
    <div class="d-flex flex-column">
        <div class="d-flex mb-5 mt-5">
            <div>
                <h1>Tris</h1>
                <p>Se non sai come si gioca Google ti può aiutare sicuramente!</p>
            </div>
            <div class="ml-auto">
                <?php if ($logged) { ?>
                    <a href="logout.php">Esci</a>
                <?php } ?>
            </div>
        </div>
        <?php if (!$logged) { ?>
            <?php include __DIR__ . "/parts/player.php"; ?>
        <?php } else { ?>
            <?php include __DIR__ . "/parts/game.php"; ?>
        <?php } ?>
    </div>
</div>


<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
        crossorigin="anonymous"></script>

<script>
    var currentPlayer = true;
    var players = {
        true: {
            title: 'player_one',
            name: '<?= $_SESSION['player_one']['name'] ?>',
            sign: '<img src="svg/x.svg" alt="x" class="w-50 m-auto"/>',
        },
        false: {
            title: 'player_two',
            name: '<?= $_SESSION['player_two']['name'] ?>',
            sign: '<img src="svg/o.svg" alt="o" class="w-50 m-auto"/>',
        },
    };
    var won = false;

    $(document)
        .on('click', '.cell', function () {
            if (won) {
                return;
            }

            var el = $(this);

            // Se la cella è occupata
            if (el.hasClass("used")) {
                // Mi fermo
                return;
            } else {
                // Altrimenti la rendo occupata
                el.addClass("used");
            }

            el.html(players[currentPlayer].sign);

            $(document)
                .trigger('check-win');

            if (won) {
                return;
            }

            // Cambio turno
            currentPlayer = !currentPlayer;

            // Aggiorna messaggio
            $(document)
                .trigger('reload-round');
        })
        .on('reload-round', function () {
            $("#game #round").html("É il turno di \"" + players[currentPlayer].name + "\"");
        })
        .on('check-win', function () {
            var table = $("#table");

            // Creo funzione di supporto
            var trovaCella = function (table, coordRow, coordCol) {
                return table.find("[data-row=" + coordRow + "][data-col=" + coordCol + "]");
            }

            var controlla = function (cell1, cell2, cell3) {
                return cell1.html() === cell2.html() && cell2.html() === cell3.html() && cell3.html() !== "";
            }

            if (
                // Controlla orizzontali:
                controlla(trovaCella(table, 0, 0), trovaCella(table, 0, 1), trovaCella(table, 0, 2)) ||
                controlla(trovaCella(table, 1, 0), trovaCella(table, 1, 1), trovaCella(table, 1, 2)) ||
                controlla(trovaCella(table, 2, 0), trovaCella(table, 2, 1), trovaCella(table, 2, 2)) ||
                // Controlla verticali:
                controlla(trovaCella(table, 0, 0), trovaCella(table, 1, 0), trovaCella(table, 2, 0)) ||
                controlla(trovaCella(table, 0, 1), trovaCella(table, 1, 1), trovaCella(table, 2, 1)) ||
                controlla(trovaCella(table, 0, 2), trovaCella(table, 1, 2), trovaCella(table, 2, 2)) ||
                // Controlla obliqui:
                controlla(trovaCella(table, 0, 0), trovaCella(table, 1, 1), trovaCella(table, 2, 2)) ||
                controlla(trovaCella(table, 2, 0), trovaCella(table, 1, 1), trovaCella(table, 0, 2))
            ) {
                // Eureka
                $("#game #round").html("Ha vinto \"" + players[currentPlayer].name + "\" <a href=\"add_win.php?winner=" + players[currentPlayer].title + "\">Registra vincita</a>");

                won = true;
            }
        })
        .trigger('reload-round');
</script>
</body>
</html>